import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        ServerConnection serverConnection = new ServerConnection();
        serverConnection.initializeConnection();

        ExecuteCommands executeCommands = new ExecuteCommands();
        executeCommands.executeCommand();
    }

}
