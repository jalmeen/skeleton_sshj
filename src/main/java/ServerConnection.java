import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.connection.ConnectionException;
import net.schmizz.sshj.connection.channel.direct.Session;
import net.schmizz.sshj.transport.TransportException;

import java.io.IOException;

public class ServerConnection {

    protected  SSHClient sshClient = new SSHClient();
    protected Session session;

    public ServerConnection() throws ConnectionException, TransportException {
    }

    public void initializeConnection() throws IOException {

        sshClient.loadKnownHosts();

        sshClient.connect("192.168.200.8");

        sshClient.authPublickey("datreon");
        session = sshClient.startSession();

        if(sshClient.isConnected()){
            System.out.println("Connected successfully!");
        }
        else {
            System.out.println("ServerConnection not successfull!");
        }
    }
}
