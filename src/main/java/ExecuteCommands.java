import net.schmizz.sshj.common.IOUtils;
import net.schmizz.sshj.connection.ConnectionException;
import net.schmizz.sshj.connection.channel.direct.Session;
import net.schmizz.sshj.transport.TransportException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class ExecuteCommands extends ServerConnection {
    protected Session.Command cmd;

    public ExecuteCommands() throws ConnectionException, TransportException {
    }

    public void executeCommand() throws IOException {

        cmd = session.exec("df -h");

        System.out.println(IOUtils.readFully(cmd.getInputStream()).toString());
        cmd.join(1, TimeUnit.SECONDS);

        session.close();

        sshClient.disconnect();

    }
}
